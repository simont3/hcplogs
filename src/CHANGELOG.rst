Release History
===============

**2.0.9 2016-02-04**

    *   Added info about error handling in the documenation

**2.0.8 2016-01-13**

    *   Changed default settings in the template configuration file

    *   Fixed a bug that caused hcplogs to fail in case there are Nodes with a
        backend IP address with a non-3 digit last octet

**2.0.7 2015-12-08**

    *   Added a documentation section that describes the log types and their
        columns

**2.0.6 2015-11-08**

    *   After downloads are finished, we now *cancel* the download facility
        within HCP

**2.0.5 2015-10-20**

    *   Documentation fixes

**2.0.4 2015-10-15**

    *   Added logo to documentation, trademark etc.

**2.0.3 2015-10-14**

    *   Changed documentation / installation

**2.0.2 2015-10-11**

    *   Removed unnecessary variables from hcplogs.init.py

**2.0.1 2015-10-11**

    *   Changed entrypoint script to allow for *console_script*
        configuration, to allow the tool to be directly executable
    *   Updated documenation to explain source installation via pip and/or
        gitlab

**2.0.0 2015-10-09**

    *   Created the tool from scratch, as the existing version 1 got
        obsolete when the log download endpoint was invented in HCP 7.2



