Archive Structure
=================

The collected log files are stored in a way that makes it somewhat easy to
process them later on.

::

    [... archive store][path]/
        [target ...][folder]/
            <node IP address>/
                <year>/
                    admin_request/
                        YYYYMMDD-HHMM.log.*
                        ...
                    http_gateway_request/
                        YYYYMMDD-HHMM.log.*
                        ...
                    mapi_gateway_request/
                        YYYYMMDD-HHMM.log.*
                        ...



**Example:**

::

    /tmp/hcplogs/
        hcp72/
            192.168.0.176/
                2015/
                    admin_request/
                        20151002-1124.log.0
                        ...
                        20151007-0532.log.0
                    http_gateway_request/
                        20151002-1124.log.0
                        ...
                        20151007-0532.log.0
                    mapi_gateway_request/
                        20151002-1124.log.0
                        ...
                        20151007-0532.log.0
            192.168.0.177/
            192.168.0.178/
            192.168.0.179/
        hcp73/
            192.168.0.180/
                2015/
                    admin_request/
                    http_gateway_request/
                    mapi_gateway_request/
            192.168.0.181/
            192.168.0.182/
            192.168.0.183/

