Configuration
=============

The tool is configurable using a basic configuration language providing a
structure similar to what’s found in Microsoft Windows INI files.

The order of the sections, and the order of the items within a section isn't
relevant. Nevertheless it's suggested to leave all sections in the config
file and simply switch unneeded features to ``no`` or ``off``

..  Warning::

    As the configuration file contains user credentials, make sure that
    it is stored at a secure location with propper permission settings,
    **not to disclose it to nosy eyes!**

    The config file must be write-able by the tool itself, as it uses it to
    remember state between its runs.


The configuration file explained
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


**HCP system(s) to collect from**

    One or more HCP systems can be configured for log collection by adding
    more than a single ``[target ...]`` header. Make sure to replace ``...``
    by an unique identifier for each target section!

    *   **fqdn** needs to start with ``admin.``; using an IP address is
        *not* supported.
    *   **user** needs to be a system level user, having the *Admin* and
        *Monitor* role.
    *   **folder** is the target folder below the *path* specified in the
        ``[... archive store]`` section.
    *   **last collected** will be used by the tool to remember the last
        collection date

    ..  code-block:: text
        :emphasize-lines: 2,3,5,6,9,10,12,13

        [target hcp72]
        fqdn = admin.hcp72.domain.com
        user = logmon
        password = secret
        folder = hcp72
        last collected =

        [target hcp73]
        fqdn = admin.hcp73.domain.com
        user = logmon
        password = secret
        folder = hcp73
        last collected =

**Log type selection**

    The ``[access log]`` section allows to select the required logs.

    *   **access** - user requests
    *   **admin** - access to the Management Consoles
    *   **mapi** - requests to the Management API
    *   **omit empty** - 0-byte files will be skipped if set to ``yes``

    ..  code-block:: text
        :emphasize-lines: 2-4,6

        [access logs]
        access = yes
        admin = yes
        mapi = yes
        # do not transfer empty logfiles
        omit empty = yes

**Storage locations**

    The ``[local archive store]`` section defines where to store the
    downloaded logs locally.

    ..  code-block:: text
        :emphasize-lines: 2,4

        [local archive store]
        enable = yes
        # path should to be an absolute path
        path = /tmp/hcplogs


    The ``[compliant archive store]`` section defines where to archive the
    downloaded logs on an HCP system.

    *   **path** needs to be full qualified - including http or https and
        the target folder within the namespace
    *   **user** needs to have write permission for the namespace
    *   **retention** can be any valid retention string, as described in the
        *HCP - Using a Namespace* manual (0 means *deletion allowed*)

    ..  code-block:: text
        :emphasize-lines: 2,4,9

        [compliant archive store]
        enable = yes
        # path needs to be a full qualified Namespace and folder
        path = https://namespace.tenant.hcp72.domain.com/rest/accesslogs
        # a user having write permission to the namespace
        user = n
        password = secret
        # retention needs to be a valid HCP retention string
        retention = 0

**Logging**

    The ``[logging ]`` section defines where the tool writes its output to.
    If logging to file is enabled, logfiles are auto-rotated and limited
    to the given number of backups.

    *   **status query** tells after how many seconds a status update shall
        be logged while HCP is preparing the logs for download.

    *   **debug** enables more detailed and *very* chatty logging.

    ..  code-block:: text
        :emphasize-lines: 7,8

        [logging]
        log to stdout = yes
        log to file = yes
        logfile = /tmp/hcplogs/_hcplogs.log
        rotateMB = 10
        backups = 9
        status query = 10
        debug = no

**Temporary storage**

    This section defines where temporary files will be stored. This is
    needed for unpacking and decompressing the downloaded file, and the
    preparation for archiving. The content will be deleted while the tool
    finishs.

    ..  code-block:: text
        :emphasize-lines: 4

        [temporary store]
        # used for download/de-compression and handling the logs
        # will be cleaned up when the tool has finished its work
        tempdir = .

