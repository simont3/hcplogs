Usage
=====

**HCP access log collector** is a command line tool.

::

    $ hcplogs --help
    usage: hcplogs [-h] [--version] [-i INI]

    hcplogs downloads access logs for the REST-based interfaces
    (native/HS3/HSwift) from HCP systems. It keeps a timestamp of the last
    successfully downloaded logs and excludes everything older than it when
    running hcplogs again.

    optional arguments:
      -h, --help  show this help message and exit
      --version   show program's version number and exit
      -i INI      path/name of the ini-file containing the configuration (defaults
                  to the current directory)


A single argument, ``-i configfile.ini`` is relevant - it points to the
configuration file that specifies what the tool shall do.

..  Tip::

    If you don't have a configuration file yet, just run the tool without
    arguments; it will create a template config file for you::

        $ hcplogs
        A configuration file is not available.
        Do you want me to create a template for you (y/n)? y
        Creation of template config file "hcplogs_config.ini" was successfull
                You need to edit it to fit your needs!

**Make sure you edit the template file to fit your needs!**


**A run's output**

::

    11/08 13:01:30 [INFO  ] Started run (user "sm")
    11/08 13:01:30 [INFO  ] log prepare started for admin.hcp72.archivas.com (10/8/2015 - 11/8/2015)
    11/08 13:01:30 [INFO  ] log prepare started for admin.hcp73.archivas.com (10/8/2015 - 11/8/2015)
    11/08 13:01:31 [INFO  ] status for target hcp72: preparing
    11/08 13:01:31 [INFO  ] status for target hcp73: preparing
    11/08 13:01:42 [INFO  ] status for target hcp72: preparing
    11/08 13:01:42 [INFO  ] status for target hcp73: preparing
    11/08 13:01:53 [INFO  ] status for target hcp72: preparing
    11/08 13:01:53 [INFO  ] status for target hcp73: preparing
    11/08 13:02:04 [INFO  ] status for target hcp72: preparing
    11/08 13:02:05 [INFO  ] status for target hcp73: preparing
    11/08 13:02:15 [INFO  ] status for target hcp72: preparing
    11/08 13:02:16 [INFO  ] status for target hcp73: prepared
    11/08 13:02:26 [INFO  ] status for target hcp72: preparing
    11/08 13:02:37 [INFO  ] status for target hcp72: prepared
    11/08 13:02:37 [INFO  ] starting download for target hcp72
    11/08 13:02:48 [INFO  ] unpacking downloaded logs for target hcp72
    11/08 13:02:59 [INFO  ] starting local transfer for node 192.168.0.178 of target hcp72
    11/08 13:03:04 [INFO  ] done: transfer for node 192.168.0.178 of admin.hcp72.archivas.com (success/fail: 203/0)
    11/08 13:03:16 [INFO  ] starting local transfer for node 192.168.0.179 of target hcp72
    11/08 13:03:21 [INFO  ] done: transfer for node 192.168.0.179 of admin.hcp72.archivas.com (success/fail: 209/0)
    11/08 13:03:33 [INFO  ] starting local transfer for node 192.168.0.176 of target hcp72
    11/08 13:03:39 [INFO  ] done: transfer for node 192.168.0.176 of admin.hcp72.archivas.com (success/fail: 203/0)
    11/08 13:03:51 [INFO  ] starting local transfer for node 192.168.0.177 of target hcp72
    11/08 13:03:57 [INFO  ] done: transfer for node 192.168.0.177 of admin.hcp72.archivas.com (success/fail: 203/0)
    11/08 13:03:57 [INFO  ] starting download for target hcp73
    11/08 13:04:13 [INFO  ] unpacking downloaded logs for target hcp73
    11/08 13:04:14 [INFO  ] starting local transfer for node 192.168.0.182 of target hcp73
    11/08 13:04:14 [INFO  ] done: transfer for node 192.168.0.182 of admin.hcp73.archivas.com (success/fail: 20/0)
    11/08 13:04:14 [INFO  ] starting local transfer for node 192.168.0.183 of target hcp73
    11/08 13:04:14 [INFO  ] done: transfer for node 192.168.0.183 of admin.hcp73.archivas.com (success/fail: 20/0)
    11/08 13:04:15 [INFO  ] starting local transfer for node 192.168.0.180 of target hcp73
    11/08 13:04:15 [INFO  ] done: transfer for node 192.168.0.180 of admin.hcp73.archivas.com (success/fail: 21/0)
    11/08 13:04:16 [INFO  ] starting local transfer for node 192.168.0.181 of target hcp73
    11/08 13:04:16 [INFO  ] done: transfer for node 192.168.0.181 of admin.hcp73.archivas.com (success/fail: 20/0)
    11/08 13:04:16 [INFO  ] Finished run (user "sm")

