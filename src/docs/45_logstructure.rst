Log record structure
====================

Types of logs
-------------

**hcplogs** collects three different types of access logs (depending on how it
is configured):

    *   *admin_request*

        Log records originated by the use of the System or Tenant Management
        Consoles

    *   *http_gateway_log*

        Log records originated by data access through one of the REST-based
        gateways (native/REST, HSwift, HS3)

    *   mapi_gateway_request

        Log records originated by requests made to the Management API REST
        gateway


Examples
--------

These examples are from the *http_gateway_log*; examples from *admin_request*
and *mapi_gateway_request* are not shown here, as they are very similar to the
native/REST log records shown.

*   native/REST request::

        192.168.0.110 - filesUser [08/Dec/2015:07:29:05 +0100] \
            "GET /rest/files/testfile/1.1" \
            200 1024 files.aw21 9

*   HSwift request::

        192.168.0.220 - swiftuser [07/Dec/2015:21:14:38 +0100] \
            "GET /swift/v1/swifttest/test1?limit=10000 HTTP/1.1" \
            200 530000 test1.swifttest 5750

*   HS3 request::

        192.168.0.43 - s3user [08/Dec/2015:07:28:44 +0100] \
            "GET /73-created-01?prefix=&max-keys=1000&delimiter=%2F HTTP/1.1" \
            200 575 xyz@hs3 166

Columns explained
-----------------

+--------+---------------+-------------------------------------------------------+
| column | content       | description                                           |
+========+===============+=======================================================+
| 1      | 192.168.0.110 | the clients IP address                                |
+--------+---------------+-------------------------------------------------------+
| 2      | \-            | not used, always '-'                                  |
+--------+---------------+-------------------------------------------------------+
| 3      | filesUser     | userId used by the client for this request            |
+--------+---------------+-------------------------------------------------------+
| 4      | [08/Dec/...   | timestamp                                             |
+--------+---------------+-------------------------------------------------------+
| 5      | "GET ...      | | **the request made by the client**                  |
|        |               | |                                                     |
|        |               | | "/rest/..." --> native/REST                         |
|        |               | |                                                     |
|        |               | | "/swift/v1/<account>/<container>/..." --> HSwift    |
|        |               | | <account> maps to Tenant (or Tenant-Id if           |
|        |               | | Keystone is used for authentication),               |
|        |               | | <container> maps to Namespace                       |
|        |               | |                                                     |
|        |               | | <bucket>/..." --> HS3                               |
|        |               | | <bucket> maps to Namespace                          |
+--------+---------------+-------------------------------------------------------+
| 6      | 200           | http returncode                                       |
+--------+---------------+-------------------------------------------------------+
| 7      | 1024          | size of returned content                              |
+--------+---------------+-------------------------------------------------------+
| 8      | files.aw21    | | <namespace>.<tenant< (native/REST) or               |
|        |               | | <tenant>.<container> (HSwift)                       |
|        |               | |                                                     |
|        | xyz\@hs3      | | <tenant> @ HS3 gateway                              |
+--------+---------------+-------------------------------------------------------+
| 9      | 9             | HCP internal latency (ms)                             |
+--------+---------------+-------------------------------------------------------+

