Setup
=====


Pre-requisites
--------------

*   HCP running at least version 7.2

*   An system-level admin account having at least the *Admin* and *Monitor*
    roles

*   MAPI enabled at sytem-level

*   If the logs are to be archived to an HCP Namespace: a data access user
    having at least write access to that Namespace


Dependencies
------------

You need to have at least Python 3.4.3 installed to run **hcplogs**.

It depends on this package, which will be auto-installed if not
already available:

*   `hcpsdk <http://simont3.github.io/hcpsdk/>`_ -  Used for access to
    HCP.

You might want to use a virtual environment to fence the dependency
from your primary Python environment...


Installation
------------

**Make sure you have Python 3.4.3 (or better) installed**

In case it's not installed, get it
`here <https://www.python.org/downloads/>`_.

There are two ways to install **hcplogs**:

1.  system-wide

    *   Install **hcplogs** by running:

        ..  code-block:: text
            :emphasize-lines: 1

            $ pip install hcplogs

    -or-

    *   Get the source from
        `gitlab.com <https://gitlab.com/simont3/hcplogs>`_  either

        *   by downloading the source archive, or

        *   by cloning the repository:

            ..  code-block:: text
                :emphasize-lines: 1

                $ git clone https://gitlab.com/simont3/hcplogs


    *   Install locally, including the dependency:

        ..  code-block:: text
            :emphasize-lines: 1

            $ python setup.py install


2.   in a virtual environment

    **WINDOWS**

    *   Create a fresh virtual environment:

        ..  code-block:: text
            :emphasize-lines: 1

            C:\>\Python35\Tools\scripts\pyvenv.py c:\temp\_venv_hcplogs

    *   Activate the virtual environment:

        ..  code-block:: text
            :emphasize-lines: 1,3

            C:\>cd temp

            C:\temp>\temp\_venv_hcplogs\Scripts\activate.bat
            (_venv_hcplogs) C:\temp>

    *   Install **hcplogs**:

        ..  code-block:: text
            :emphasize-lines: 1

            (_venv_hcplogs) C:\temp>pip install hcplogs
            Collecting hcplogs
              Downloading hcplogs-2.0.2.tar.gz
            Collecting hcpsdk>=0.9.3.post0 (from hcplogs)
              Downloading hcpsdk-0.9.3.post4.tar.gz
            Collecting dnspython3==1.12.0 (from hcpsdk>=0.9.3.post0->hcplogs)
              Downloading dnspython3-1.12.0.zip (226kB)
                100% |################################| 229kB 1.3MB/s
            Collecting alabaster>=0.7.1 (from hcpsdk>=0.9.3.post0->hcplogs)
              Downloading alabaster-0.7.6.tar.gz
            Installing collected packages: dnspython3, alabaster, hcpsdk, hcplogs
              Running setup.py install for dnspython3
              Running setup.py install for alabaster
              Running setup.py install for hcpsdk
              Running setup.py install for hcplogs
            Successfully installed alabaster-0.7.6 dnspython3-1.12.0 hcplogs-2.0.2 hcpsdk-0.9.3.post4

    Now you can run **hcplogs** as long as you have the virtual environment
    activated:

    ..  code-block:: text
        :emphasize-lines: 1

        C:\temp>hcplogs
        A configuration file is not available.
        Do you want me to create a template for you (y/n)? y
        Creation of template config file "hcplogs_config.ini" was successfull
            You need to edit it to fit your needs!

    **Linux**

    *   Create a fresh virtual environment:

        ..  code-block:: text
            :emphasize-lines: 1

            $ pyvenv _venv_hcplogs

    *   Activate the virtual environment:

        ..  code-block:: text
            :emphasize-lines: 1

            $ source _venv_hcplogs/bin/activate


    *   Install **hcplogs**:

        ..  code-block:: text
            :emphasize-lines: 1

            (_venv_hcplogs) $ pip install hcplogs
            Collecting hcplogs
              Downloading hcplogs-2.0.4.tar.gz
            Collecting hcpsdk>=0.9.3.post0 (from hcplogs)
              Downloading hcpsdk-0.9.3.post4.tar.gz
            Collecting dnspython3==1.12.0 (from hcpsdk>=0.9.3.post0->hcplogs)
              Downloading dnspython3-1.12.0.zip (226kB)
                100% |################################| 229kB 7.4MB/s
            Collecting alabaster>=0.7.1 (from hcpsdk>=0.9.3.post0->hcplogs)
              Downloading alabaster-0.7.6.tar.gz
            Installing collected packages: dnspython3, alabaster, hcpsdk, hcplogs
              Running setup.py install for dnspython3
              Running setup.py install for alabaster
              Running setup.py install for hcpsdk
              Running setup.py install for hcplogs
            Successfully installed alabaster-0.7.6 dnspython3-1.12.0 hcplogs-2.0.4 hcpsdk-0.9.3.post4

    Now you can run **hcplogs** as long as you have the virtual environment
    activated:

    ..  code-block:: text
        :emphasize-lines: 1

        (_venv_hcplogs) $ hcplogs
        A configuration file is not available.
        Do you want me to create a template for you (y/n)? y
        Creation of template config file "hcplogs_config.ini" was successfull
                You need to edit it to fit your needs!


