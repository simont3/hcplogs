HCP access log collector (|release|)
====================================

Hitachi Content Platform (HCP) logs access to its REST-based interfaces on a
regular basis and keeps these logs for up to 90 days. While access to these
logs was possible by downloading the internal logs using the System
Management Console (SMC) since HCP version 7.0, it was a quite cumbersome
manual task to extract the logs from the provided zip-file.

HCP version 7.2 invented a Management API (MAPI) endpoint that allows to
selectively access parts (or all) of the internal logs.

The **HCP access log collector** tool concentrates on downloading the
access logs only, enabling users to collect (and archive) these logs
on a regular basis.


..  toctree::
    :maxdepth: 2

    15_installation
    20_usage
    30_config
    40_output
    45_logstructure
    50_errors
    97_changelog
    98_license
    99_about

..
    Indices and tables
    ==================

    *   :ref:`genindex`
    *   :ref:`modindex`
    *   :ref:`search`

