Error Handling
==============

**Faulty read errors**

..  code-block:: text
    :emphasize-lines: 2,3

    02/04 17:36:48 [INFO  ] starting download for target hcp72
    02/04 17:37:20 [ERROR ] download for admin.hcp72.archivas.com failed
        hint: faulty read: 'NoneType' object has no attribute 'read'

This is a good indicator that the connection between **hcplogs** and HCP has
been cut off by a third party while downloading the logs.

Check if your connection to HCP is routed through a *proxy* or
*load balancer*. Most likely, there is a setting that cuts off connections that
last longer than some seconds, even if in transaction.

As downloading the logs from HCP is not too fast, it is possible that these
settings are too short. Look out for the proxy/load balancer configuration
that is responisble for traffic to HCPs port 9090 (MAPI).


**Load Balancer settings example**

..  figure:: _static/lb_timeout.png
    :alt: map to buried treasure

    The screenshot shows the relevant panel for **Brocade's VTM**